package com.example.capstone.pizza_delivery_service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PizzaDeliveryServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PizzaDeliveryServiceApplication.class, args);
	}

}
